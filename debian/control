Source: picom
Section: x11
Priority: optional
Maintainer: Volian Developers <volian-devel@volian.org>
Uploaders: Blake Lee <blake@volian.org>
Build-Depends:	debhelper-compat (= 13),
				pkg-config,
				meson,
				asciidoc,
				docbook-xml,
				docbook-xsl,
				libxext-dev,
				libxcb1-dev,
				libxcb-damage0-dev,
				libxcb-xfixes0-dev,
				libxcb-shape0-dev,
				libxcb-render-util0-dev,
				libxcb-render0-dev,
				libxcb-randr0-dev,
				libxcb-composite0-dev,
				libxcb-image0-dev,
				libxcb-present-dev,
				libxcb-xinerama0-dev,
				libxcb-glx0-dev,
				libpixman-1-dev,
				libdbus-1-dev,
				libconfig-dev,
				libgl1-mesa-dev,
				libpcre2-dev,
				libevdev-dev,
				uthash-dev,
				libev-dev,
				libx11-xcb-dev,
				libpcre3-dev,
Standards-Version: 4.5.0
Homepage: https://github.com/yshui/picom
Vcs-Git: https://salsa.debian.org/volian-team/picom-git.git
Vcs-Browser: https://salsa.debian.org/volian-team/picom-git
Rules-Requires-Root: no

Package: picom-git
Architecture: any
Depends: python3, ${misc:Depends}, ${shlibs:Depends}
Replaces: picom
Conflicts: picom
Description: lightweight compositor for X11
 picom is a compositor for X11, based on xcompmgr. In addition to shadows,
 fading and translucency, picom implements window frame opacity control,
 inactive window transparency, and shadows on argb windows.
 .
 picom is a fork of compton as it seems to have become unmaintained.
 .
 picom-git is compiled directly from picom next branch for the latest features.
