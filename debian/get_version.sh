#!/bin/sh

# A simple script to make a version number for our package
# Used on the upstream branch so we know what commit upstream was on
# This is also going to be the tag for our Volian branch
count=$(git rev-list v8.2..HEAD | wc -l)
commit=$(git rev-parse --short=7 HEAD)
version="8.2+$count+$commit"
echo $version
